# CodeRacer_hackathon_ 

# [Ergebnisse Qualifikation und Finals](https://docs.google.com/spreadsheets/d/1VYHfpSw_Ma1mIEiNILv0Prl4COWuJflw1iML3Zlw3eI/edit#gid=827850699)

![Flyer](X_day/Codewerk_CodeRacer-Hackathon_GitLab.jpg)

## Parcours
![Flyer](Doc/BspParcours.PNG)

## Repo Overview
Repo dir | Description
---------|--------------
Arduino/libraries | arduino stuff and libraries
Doc 	| HowTo as well as doxygen documentation. View API doxygen documentation [online](https://doc.itsblue.de/Fenoglio/coderacer/Doku/Doxygen/html/) or download the API as PDF.
Install | Everything to get your dev IDE running on your PC
X_day | all information about the X-Day hackathon itself. Competition rules and pictures 

## How to start
* Start reading the [{code}racer HowTo](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/Doc/coderacer_handbuch.pdf)
* Learn about the [{code}racer API as PDF](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/Doc/coderacerAPI.pdf) or [API as HTML](https://doc.itsblue.de/Fenoglio/coderacer/Doku/Doxygen/html/)
* Download the CodeRacer Repo and install the IDE on your PC (see [{code}racer HowTo Page.8](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/Doc/coderacer_handbuch.pdf))
* Installation of used libraries (see [{code}racer HowTo Page.10](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/Doc/coderacer_handbuch.pdf))
* Compile example (esp32_coderacer_beispiel) (see [{code}racer HowTo Page.11](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/Doc/coderacer_handbuch.pdf))
* Work on your OWN {code}racer Algo
* Come to {code}racer_hackathon and run the Challenge! See below for upcoming events/dates.

## CodeRacer_hackathon_Jobwunder TU Berlin, 07.05.2019
* starting at 10:00am @ Technische Universität Berlin, Standnummer 15
* bring your own {code}racer Algo and run the challenge (see [Rules](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/X_day/Rules.pdf) and impressions from the [CodeRacer Challenge](https://photos.google.com/share/AF1QipO3Dx4p3dLuXtcd0LmBctfb6LwVPVEonxy3xjEPUoOvyPFi_XSlrooCWoWZ4KsfbA?key=R19FdFRoX0dOVmJYWEFRVzlCc25lQkNiNmNXaHBn) in Murnau this year)
* improve, change your Algo directly
* Win one of three prices!

## CodeRacer_hackathon_Campus-X-Change, 22.05.2019
* starting at 10:00am @ BTU Cottbus-Senftenberg, Gebäude VG1C, Raum 0.01
* bring your own {code}racer Algo and run the challenge (see [Rules](https://gitlab.com/Codewerk/coderacer_hackathon_x-day/blob/master/X_day/Rules.pdf) and impressions from the [CodeRacer Challenge](https://photos.google.com/share/AF1QipO3Dx4p3dLuXtcd0LmBctfb6LwVPVEonxy3xjEPUoOvyPFi_XSlrooCWoWZ4KsfbA?key=R19FdFRoX0dOVmJYWEFRVzlCc25lQkNiNmNXaHBn) in Murnau this year)
* improve, change your Algo directly
* Win one of three prices!


## Feedback/Questions
* Please write your questions/feedback to karriere@codewerk.de



