var searchData=
[
  ['drive_5fbackward',['drive_backward',['../d2/d40/group__higherlevelmeths.html#ga98aa8a5a46f769d59e6c1067c8418cfb',1,'CodeRacer::drive_backward()'],['../d2/d40/group__higherlevelmeths.html#ga6cd8356ac76360b014db6170276b6b30',1,'CodeRacer::drive_backward(uint8_t left_speed, uint8_t right_speed)']]],
  ['drive_5fforward',['drive_forward',['../d2/d40/group__higherlevelmeths.html#ga8be6a850099812153dcf5768d7fc8b8c',1,'CodeRacer::drive_forward()'],['../d2/d40/group__higherlevelmeths.html#gaae6fc379ec43cbe2cb2f63fbd12dbe0d',1,'CodeRacer::drive_forward(uint8_t left_speed, uint8_t right_speed)']]],
  ['drive_5fleft_5fspeed',['drive_left_speed',['../d0/d0a/group__lowerleveldrivesgetters.html#ga804a45724f3788fd2fdb9631c66d1377',1,'CodeRacer']]],
  ['drive_5fright_5fspeed',['drive_right_speed',['../d0/d0a/group__lowerleveldrivesgetters.html#ga09359a792e3299b1c20f6b99939ea7b3',1,'CodeRacer']]],
  ['drives_5fsettings',['drives_settings',['../d1/d8a/group__lowerleveldrivesmeths.html#ga5812df751da5b480240ccd633c515b83',1,'CodeRacer']]]
];
