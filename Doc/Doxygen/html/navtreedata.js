var NAVTREE =
[
  [ "Arduino {code}racer API", "index.html", [
    [ "Modules", "modules.html", "modules" ]
  ] ]
];

var NAVTREEINDEX =
[
"d0/d0a/group__lowerleveldrivesgetters.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';