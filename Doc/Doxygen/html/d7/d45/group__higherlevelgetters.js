var group__higherlevelgetters =
[
    [ "stopped_at_min_distance", "d7/d45/group__higherlevelgetters.html#gae8b8c0ab24ccb572281785aeca8541e1", null ],
    [ "is_driving", "d7/d45/group__higherlevelgetters.html#ga33dcd96e9b12dec794c56be85ec1ee05", null ],
    [ "turn_left_for_ms", "d7/d45/group__higherlevelgetters.html#gaf04fd16ca0e2ace656f9549c43d16459", null ],
    [ "turn_right_for_ms", "d7/d45/group__higherlevelgetters.html#gac0698f02f6a21d9d1f5b9cf2820306cf", null ],
    [ "set_inactive", "d7/d45/group__higherlevelgetters.html#ga62f1c0eea56e27d0853cb58f30eb140d", null ],
    [ "set_active", "d7/d45/group__higherlevelgetters.html#ga415c69930f291d5e06b7211b31843310", null ],
    [ "is_active", "d7/d45/group__higherlevelgetters.html#gaa0ab4d6a754a23ea13664a553bcc8ff2", null ]
];