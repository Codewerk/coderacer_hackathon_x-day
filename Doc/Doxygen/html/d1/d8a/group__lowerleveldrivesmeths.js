var group__lowerleveldrivesmeths =
[
    [ "drives_settings", "d1/d8a/group__lowerleveldrivesmeths.html#ga5812df751da5b480240ccd633c515b83", null ],
    [ "set_drives_stop_left_right", "d1/d8a/group__lowerleveldrivesmeths.html#ga61ed9e0415a62a290cc5c59a0f740304", null ],
    [ "set_drives_states_left_right", "d1/d8a/group__lowerleveldrivesmeths.html#ga802d2646d9cc0d766e1ac799c7917fa8", null ],
    [ "set_drive_left_state", "d1/d8a/group__lowerleveldrivesmeths.html#ga1eeb3cb47503c3011562f9c42828fab9", null ],
    [ "set_drive_right_state", "d1/d8a/group__lowerleveldrivesmeths.html#ga1b34ec9cee1f21cd15db310167a2faa5", null ],
    [ "set_drive_state", "d1/d8a/group__lowerleveldrivesmeths.html#ga3b69cf4a718c842fbe758d3f4267214e", null ],
    [ "set_drives_speed_left_right", "d1/d8a/group__lowerleveldrivesmeths.html#ga3d3ffb41783d34589e33cf61fed46c70", null ],
    [ "set_drive_left_speed", "d1/d8a/group__lowerleveldrivesmeths.html#ga1ee3da20ec98a821ab97ced070974861", null ],
    [ "set_drive_right_speed", "d1/d8a/group__lowerleveldrivesmeths.html#gaf8805d0d620a2fb78c576f36a2c81073", null ],
    [ "set_drive_speed", "d1/d8a/group__lowerleveldrivesmeths.html#ga5d67c84606d5b39996a99fcd6e7eb314", null ]
];