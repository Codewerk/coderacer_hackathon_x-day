var group__lowerlevelusgetters =
[
    [ "usonic_distance_cm", "group__lowerlevelusgetters.html#gad59842c14196598e55644b2a22621454", null ],
    [ "usonic_distance_us", "group__lowerlevelusgetters.html#ga917b90f21e731ef5f690d5198e7f4d3e", null ],
    [ "usonic_set_stop_distance_cm", "group__lowerlevelusgetters.html#gaa7c5a6563a5736ed38d12f616de480df", null ],
    [ "usonic_set_stop_distance_us", "group__lowerlevelusgetters.html#ga2f06c193ae86c5b1cba450caf5adf146", null ]
];