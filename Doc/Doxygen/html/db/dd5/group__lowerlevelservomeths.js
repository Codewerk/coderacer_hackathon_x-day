var group__lowerlevelservomeths =
[
    [ "servo_settings", "db/dd5/group__lowerlevelservomeths.html#ga687beb327e20f4d0541d1ac9e29c01c3", null ],
    [ "servo_sweep", "db/dd5/group__lowerlevelservomeths.html#ga1e9afe1f27dfc9796b4c9b3dba245365", null ],
    [ "servo_set_to_right", "db/dd5/group__lowerlevelservomeths.html#gaac73bf99cf2d19f7b1987156aa842b74", null ],
    [ "servo_set_to_left", "db/dd5/group__lowerlevelservomeths.html#gaef7d1903b65a0a8ab4fafdc53080b07d", null ],
    [ "servo_set_to_center", "db/dd5/group__lowerlevelservomeths.html#gad1f28aa91079e88fc3093e3074edfb32", null ],
    [ "servo_set_position_wait", "db/dd5/group__lowerlevelservomeths.html#ga0149226288bff2290d52ed1cbd674edd", null ],
    [ "servo_set_position", "db/dd5/group__lowerlevelservomeths.html#gab8831340049de6dbddeda997745725e6", null ]
];